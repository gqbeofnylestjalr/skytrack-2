import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas";

import images from "./reducers/images";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(images, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);
export default store;
