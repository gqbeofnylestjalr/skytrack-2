import { put, call, fork, takeEvery, all } from "redux-saga/effects";
import * as actions from "../actions/images";
import { api } from "../../services";
import * as t from "../actionTypes/images";

function* loadImage() {
  try {
    yield put(actions.imageRequest());
    const responce = yield call(api.getImage);
    if (responce.status === "success")
      yield put(actions.imageSuccess(responce.data));
    else yield put(actions.imageFailure(responce.message));
  } catch (error) {
    yield put(actions.imageFailure(error.message));
  }
}

export function* watchLoadImage() {
  yield takeEvery(t.IMAGE_START, loadImage);
}

export default function* root() {
  yield all([fork(watchLoadImage)]);
}
