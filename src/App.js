import React from "react";
import Routes from "./routes/index";
import Header from "./components/Header";

class App extends React.Component {
  render() {
    return (
      <div className="layout">
        <Header />
        <main>
          <Routes {...this.props} />
        </main>
      </div>
    );
  }
}

export default App;
